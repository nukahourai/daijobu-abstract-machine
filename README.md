# Daijobu Abstract Machine

Daijobu Abstract Machine is a collection of AMXX plugins that were made for use in our game servers. These are now available for the general public.

Some of the included plugins in the repository are:
  - Advanced Survival Mode
     - AMXX plugin to enable Survival Support in all maps via a DoD-like wave system.
  - Gabe Iggy's XP Mod (GIXPM)
     - Rewritten of the widely popular but poorly supported SCXPM, which uses the most recent AMXX features and modules.

### Installation

You should compile all the plugins by yourself as we won't include compiled binaries. AMXX 1.8.3 is a requirement.

### Development

Please help us by creating pull requests. If you want to be added to the actual developement team please join our Discord server.

### License

All .sma files hosted on this repository are (unless explicitly stated otherwise) licenced by the **GNU Affero General Public License v3**.

The AGPL license differs from the other GNU licenses in that it was built for network software. You can distribute modified versions if you keep track of the changes and the date you made them. As per usual with GNU licenses, you must license derivatives under AGPL. It provides the same restrictions and freedoms as the GPLv3 but with an additional clause which makes it so that source code must be distributed along with web publication. If you are hosting modified plugins licenced under the Affero Licence in your server, **you must** distribute the source code by another medium.